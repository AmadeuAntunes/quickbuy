﻿namespace QuickBuy.Dominio.Entidades
{
    using System;
    using System.Collections.Generic;

    public class Pedido
    {
        public int Id { get; set; }
        public DateTime DataPedido { get; set; }
        public int UsuarioId { get; set; }
        public ICollection<ItemPedido> ItemsPedido { get; set; }
    }
}
