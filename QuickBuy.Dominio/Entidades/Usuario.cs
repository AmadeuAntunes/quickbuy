﻿

namespace QuickBuy.Dominio.Entidades
{
    using System.Collections.Generic;
    public class Usuario : Entidade
    {
        public int Id { get; set; }
        public int Email { get; set; }
        public int Senha { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }

        public ICollection<Pedido> Pedidos { get; set; }
    }
}   
